describe packages(/docker-ce/) do
  its('statuses') { should cmp 'installed' }
  its('versions') { should cmp '18.03.0~ce-0~debian' }
end


describe service('docker') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
